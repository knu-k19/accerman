#include <iostream>
using namespace std;

const int STACK_SIZE = 100;
struct StackElement {
  int ns, xs, ys;
};
struct Stack {
	StackElement elements[STACK_SIZE];
	int top;
};

int accerman(int, int, int);
int accermanForSmallValues(int, int);
void addToStack(Stack&, int, int, int);
void readFromStack(Stack, int&, int&, int&);
bool isStackEmpty(Stack);
bool isStackFull(Stack);
void removeTopElementFromStack(Stack&);

int main(){
	int n, x, y;
	cout << "Function Accerman n= x= y=" << endl;
	cin >> n >> x >> y;
	cout << endl;
	cout << "Acc = " << accerman(n, x, y) << endl;
	cin.get();
	return 0;
}

//--------------------------------
//������������ ������� ���������
int accerman(int ni, int xi, int yi) {
	int t, n, x, y;
	Stack stack;
	stack.top = STACK_SIZE;
	addToStack(stack, ni, xi, yi);
	do {
		readFromStack(stack, n, x, y);
		if (n&&y) {
			addToStack(stack, n, x, y-1);
		} else {
			t = accermanForSmallValues(n, x);
			removeTopElementFromStack(stack);
			if (!isStackEmpty(stack)) {
				readFromStack(stack, n, x, y);
				removeTopElementFromStack(stack);
				addToStack(stack, n-1, t, x);
			}
		}
	} while (!isStackEmpty(stack));
	return t;
}

//--------------------------------
//������� ��������� ��� n=0|y=0
int accermanForSmallValues(int n, int x) {
	switch (n) {
		case 0: return x+1;
		case 1: return x;
		case 2: return 0;
		case 3: return 1;
		default: return 2;
	}
}
//--------------------------------
//��������� � ���� st ���� �����
void addToStack(Stack &stack, int ni, int xi, int yi) {
	if (isStackFull(stack)) {
		cout << "Stack is full" << endl;
		return;
	}

	stack.top--;
	stack.elements[stack.top].ns = ni;
	stack.elements[stack.top].xs = xi;
	stack.elements[stack.top].ys = yi;
}

//--------------------------------
//������� � ����� st �����
void readFromStack(Stack stack, int &n, int &x, int &y) {
	n = stack.elements[stack.top].ns;
	x = stack.elements[stack.top].xs;
	y = stack.elements[stack.top].ys;
}

bool isStackEmpty(Stack stack) {
	return (stack.top == STACK_SIZE);
}

bool isStackFull(Stack stack) {
	return (stack.top == 0);
}

void removeTopElementFromStack(Stack &stack) {
	stack.top++;
}
